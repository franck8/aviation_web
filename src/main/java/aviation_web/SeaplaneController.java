package aviation_web;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SeaplaneController {

	private final Logger log = LoggerFactory.getLogger(NewController.class);

	@RequestMapping("/seaplanes")
	public String show(Model m) {

		m.addAttribute("l",source.getList()); // le template recoit une liste d'objets

		return "seaplanes";
	}

	private SeaplaneSource source;

	@Autowired
	public void setSource(SeaplaneSource source) {
		this.source = source;
	}

}
