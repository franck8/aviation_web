package aviation_web;

public class Seaplane {

	private boolean outshore;
	private String model;
	private String registration;

	Seaplane() {
		this(true, null, null);
	}

	public Seaplane(boolean outshore, String model, String registration) {
		this.outshore = outshore;
		this.model = model;
		this.registration = registration;

	}

	public boolean isOutshore() {
		return outshore;
	}

	public void setOutshore(boolean outshore) {
		this.outshore = outshore;
	}

	public String getRegistration() {
		return registration;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

}