package aviation_web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class SeaplaneSource {

	public List<Seaplane> getList() {

		List<Seaplane> l = new ArrayList<Seaplane>();
		l.add(new Seaplane(true, "H125", "Fr1234"));
		l.add(new Seaplane(false, "H888", "Fr1234"));
		l.add(new Seaplane(true, "H245", "Fr1234"));
		l.add(new Seaplane(true, "H136", "Fr1234"));

		return l;

	}
}
