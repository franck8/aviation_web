package aviation_web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

													// @Controller :     annotation pour avoir le controler et cette annotation permettra de passer sur tousles autres controller de la classe
												    // @RequestMapping : annotation pour gerer le lien entre l'URL et le controleur 
@Controller 
public class NewController { 						// "comme une tour de controle"
	private final Logger log = LoggerFactory.getLogger(NewController.class); // nouvelle attribut local via une usine a log de slf4j; 
@RequestMapping("/news")	
	public String show(Model model) {				// renvoit un string car on veut le nom de la template - PERMET LE LIEN ENTRE LE CONTROLEUR ET LE TEMPLATE. 
	log.info("Show news"+ newsTitle);				
	model.addAttribute("title", newsTitle);			// Model est un dictionnaire vide (on va le remplir pour etre envoyer vers le template (MODEL = TRANSPORTEUR)
	model.addAttribute("text", newsText);
		return "news"; 
													// permet d'avoir les messages d'erreurs chosit a un niveau d'infos	
	}
@Value("${news.title:''}")							// annotation pour l'injection des valeurs
	private String newsTitle; 						// attributs creer pour recevoir les valeurs dans properties 
@Value("${news.text:''}")							// annotation pour l'injection des valeurs
	private String newsText;
														
}


